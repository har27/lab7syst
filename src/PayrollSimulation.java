/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */

/**
 *
 * @author hptop
 */
public class PayrollSimulation {

    public static void main(String[] args){
         
         Employee emp = new Employee("David",5,60);
         Employee mng = new Manager("Teja", 60,80,70);
         
         System.out.println(emp.getName() +"'s payvheck= $"+ emp.calculatePay());
         
         System.out.println("\n");
         
         System.out.println(mng.getName() +"'s paycheck = $"+ mng.calculatePay());
     }

    
}
