/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author hptop
 */
public class Employee {
    protected String name;
    protected double hourlyWage;
    protected double hoursWorked;
    
    public Employee(String name , double hourlyWage, double hoursWorked){
        this.name=name;
        this.hourlyWage=hourlyWage;
        this.hoursWorked= hoursWorked;
    }
    
    public String getName(){
        return name;
    }
    
    public double getHourlyWage(){
        return hourlyWage;
    }
    
    public double getHoursWorked(){
        return hoursWorked;
    }
    
    public double calculatePay(){
        return getHourlyWage()*getHoursWorked();
    }

  
    
    
}
